// 插件核心代码
let McLazyLoad = function (param) {
  param = param || {};
  this.area = param.area && document.querySelector(param.area) ? document.querySelector(param.area) : window;
  this.doms = document.querySelectorAll(param.el || '.mc-lazyload') || [];
  this.size = this.doms.length;
  this.source = param.source || 'data-src';
  this.offset = window.innerHeight * (param.offset || 1);
  this.on = Object.assign({ loaded: () => { } }, param.on || {});

  // 兼容处理
  let requestAnimFrame = (function () { return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (callback) { setTimeout(callback, 1000 / 60); }; })();

  // 添加标识
  this.doms.forEach((dom) => {
    dom.style.objectFit = 'scale-down';
    dom.setAttribute('data-load', 'ing');
  });

  // 任务排队
  let tacking = 1;
  let tacked = 0;

  // 核心代码
  let core = () => {
    tacked = 0;
    for (let i = 0; i < this.size; i++) {
      let dom = this.doms[i];
      tacked = tacked + (dom._inviewStatus_ ? 1 : 0);
      ((dom) => {
        let domRect = dom.getBoundingClientRect();
        if (domRect.top < this.offset) {
          if (!dom._inviewStatus_) {
            dom.lazyImg = document.createElement('img');
            dom.lazyImg.src = dom.getAttribute(this.source);
            dom.lazyImg.addEventListener('load', () => {
              dom.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACAQMAAABIeJ9nAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjAAIAAAQAASDSLW8AAAAASUVORK5CYII=';
              setTimeout(() => {
                dom.src = dom.lazyImg.src;
                dom.removeAttribute('data-load');
                dom.style.objectFit = '';
              }, 10);
              this.on.loaded(dom);
            });
            dom._inviewStatus_ = 1;
          }
        }
      })(dom);
    }
    tacking = 1;
  };
  core();
  this.area.addEventListener('scroll', () => {
    requestAnimFrame(() => {
      if (tacking && tacked < this.size) {
        tacking = 0;
        core();
      } else {
        return false;
      }
    });

  });
};

export default McLazyLoad;
