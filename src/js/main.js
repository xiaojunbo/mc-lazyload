import '../css/main.scss';

// 代码着色插件
import McTinting from 'mc-tinting';

new McTinting();

// 楼层埋点插件
import McLazyLoad from '../../lib/mc-lazyload';

// 楼层埋点
new McLazyLoad({
  el: '.lazyload',
  offset: .5,
  // area:'#scroll'
});

